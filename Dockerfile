FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code /app/data /run/yarn /run/cache /run/rallly
WORKDIR /app/code

# Rallly version - NO DICE, NO TAGS OR RELEASES AVAILABLE, arbitrary number in case of updates
ARG VERSION=v0.1.0

# get the thing
RUN git clone https://github.com/lukevella/rallly .

# install dependencies
RUN yarn

# file and folder setup
RUN ln -s /app/data/env /app/code/.env && \
#    ln -s /app/data/.next /app/code/.next && \ # build doesn't work with symlinks
    rm -rf /home/cloudron/.yarn && ln -s /run/yarn /home/cloudron/.yarn && \
    rm -rf /home/cloudron/.cache && ln -s /run/cache /home/cloudron/.cache && \
    ln -s /app/data/.yarnrc /home/cloudron/.yarnrc

RUN chown -R cloudron:cloudron /app/code /app/data /home/cloudron /run/rallly

COPY start.sh /app/pkg/

# was required when building on a windows machine, can probably be removed
RUN chmod +x /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
