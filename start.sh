#!/bin/bash

set -eux

echo "=> Creating directories"
mkdir -p /app/data/ /run/yarn /run/cache/ /run/rallly

if [[ ! -f /app/data/env ]]; then
    echo "=> First run"
    cp /app/code/sample.env /app/data/env

    # set random encryption key
    export RALLYPWD="$(openssl rand -base64 32)"

    # update environment
    sed -e "s,^NEXT_PUBLIC_BASE_URL=.*,NEXT_PUBLIC_BASE_URL=${CLOUDRON_APP_ORIGIN}," \
        -e "s,^DATABASE_URL=.*,DATABASE_URL=postgres://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}," \
        -e "s,^SECRET_PASSWORD=.*,SECRET_PASSWORD='${RALLYPWD}'," \
        -e "s,^SUPPORT_EMAIL=.*,SUPPORT_EMAIL=${CLOUDRON_MAIL_FROM}," \
        -e "s,^SMTP_HOST=.*,SMTP_HOST={CLOUDRON_MAIL_SMTP_SERVER}," \
        -e "s,^SMTP_PORT=.*,SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}," \
        -e "s,^SMTP_SECURE=.*,SMTP_SECURE=false," \
        -e "s,^SMTP_USER=.*,SMTP_USER=${CLOUDRON_MAIL_SMTP_USERNAME}," \
        -e "s,^SMTP_PWD=.*,SMTP_PWD=${CLOUDRON_MAIL_SMTP_PASSWORD}," \
        -i /app/data/env

    cd /run/rallly
    cp -r /app/code/. /run/rallly
    echo "=> prisma migrate"
    /usr/local/bin/gosu cloudron:cloudron yarn prisma migrate deploy
    echo "=> yarn build"
    /usr/local/bin/gosu cloudron:cloudron yarn build

fi

echo "=> Updating environment"
sed -e "s,^NEXT_PUBLIC_BASE_URL=.*,NEXT_PUBLIC_BASE_URL=${CLOUDRON_APP_ORIGIN}," \
    -e "s,^DATABASE_URL=.*,DATABASE_URL=postgres://${CLOUDRON_POSTGRESQL_USERNAME}:${CLOUDRON_POSTGRESQL_PASSWORD}@${CLOUDRON_POSTGRESQL_HOST}:${CLOUDRON_POSTGRESQL_PORT}/${CLOUDRON_POSTGRESQL_DATABASE}," \
    -e "s,^SUPPORT_EMAIL=.*,SUPPORT_EMAIL=${CLOUDRON_MAIL_FROM}," \
    -e "s,^SMTP_HOST=.*,SMTP_HOST=${CLOUDRON_MAIL_SMTP_SERVER}," \
    -e "s,^SMTP_PORT=.*,SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}," \
    -e "s,^SMTP_SECURE=.*,SMTP_SECURE=false," \
    -e "s,^SMTP_USER=.*,SMTP_USER=${CLOUDRON_MAIL_SMTP_USERNAME}," \
    -e "s,^SMTP_PWD=.*,SMTP_PWD=${CLOUDRON_MAIL_SMTP_PASSWORD}," \
    -i /app/data/env


if [[ ! -f /run/rallly/.env ]]; then
    echo "=> Config missing, rebuilding app!"
    cd /run/rallly
    cp -r /app/code/. /run/rallly
    echo "=> prisma migrate"
    /usr/local/bin/gosu cloudron:cloudron yarn prisma migrate deploy
    echo "=> yarn build"
    /usr/local/bin/gosu cloudron:cloudron yarn build
fi

echo "=> Chmodding"
chown -R cloudron:cloudron /app/data /run/

echo "==> Starting Rallly"
cd /run/rallly
exec /usr/local/bin/gosu cloudron:cloudron yarn start
